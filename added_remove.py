import glob
import subprocess
import fnmatch
import os
import commands
from openpyxl import Workbook

diffile = "/home/rrvc/Desktop/loc_diff/diffile.txt"
base_dir = "/home/rrvc/Desktop/loc_diff/emsserver_3.10beta1/"
latest_dir = "/home/rrvc/Desktop/loc_diff/emsserver/"
reg_exp = '*.html'
xlxs_name = "HMTLDiffrence.xlsx"


def with_normal(all_p, f1, f2):
    cmd = "diff %s %s > %s" % (f1, f2, diffile)
    # subprocess.check_output(cmd, shell=True)
    status, output = commands.getstatusoutput(cmd)
    f = open(diffile)

    added = 0
    deleted = 0

    for l in f:
        if ">" in l:
            added += 1
        elif "<" in l:
            deleted += 1

    return [all_p, added, deleted, (added - deleted)]


def with_globe():
    all_base_py = []
    for root, dirnames, filenames in os.walk(base_dir):
        for filename in fnmatch.filter(filenames, reg_exp):
            all_base_py.append(os.path.join(root, filename))

    all_latest_py = []
    for root, dirnames, filenames in os.walk(latest_dir):
        for filename in fnmatch.filter(filenames, reg_exp):
            all_latest_py.append(os.path.join(root, filename))

    all_file = []

    base_dir_len = base_dir.__len__()
    latest_dir_len = latest_dir.__len__()

    for p in all_base_py:
        # subprocess.check_output("autopep8 -i %s" % p, shell=True)

        all_file.append(p[base_dir_len:])

    for p in all_latest_py:
        all_file.append(p[latest_dir_len:])

    book = Workbook()
    ws = book.active
    ws.title = "Diff summary"

    for all_p in set(all_file):
        base_py_file = "%s%s" % (base_dir, all_p)
        latest_py_file = "%s%s" % (latest_dir, all_p)

        base_py_file_exists = False
        latest_py_file_exists = False

        if os.path.exists(base_py_file):
            base_py_file_exists = True

        if os.path.exists(latest_py_file):
            latest_py_file_exists = True

        if base_py_file_exists and latest_py_file_exists:
            if reg_exp is  "*.html":
                subprocess.check_output("autopep8 -i %s" %
                                        base_py_file, shell=True)
                subprocess.check_output("autopep8 -i %s" %
                                        latest_py_file, shell=True)
            result = with_normal(all_p, base_py_file, latest_py_file)

        elif base_py_file_exists:
            if reg_exp is  "*.html":
                subprocess.check_output("autopep8 -i %s" %
                                        base_py_file, shell=True)

            cmd = "cat %s | wc -l" % base_py_file
            status, output = commands.getstatusoutput(cmd)

            result = (all_p, 0, output, (0 - int(output)))

        elif latest_py_file_exists:

            if reg_exp is  "*.html":
                subprocess.check_output("autopep8 -i %s" %
                                        latest_py_file, shell=True)

            cmd = "cat %s | wc -l" % latest_py_file
            status, output = commands.getstatusoutput(cmd)

            result = (all_p, output, 0, (int(output) - 0))

        ws.append(result)

    book.save(xlxs_name)


if __name__ == "__main__":
    with_globe()
