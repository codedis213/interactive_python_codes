from test import testEqual


class BinaryTree:
    def __init__(self, rootObj):
        self.key = rootObj
        self.leftChild = None
        self.rightChild = None

    def insertLeft(self, newNode):
        if self.leftChild is None:
            self.leftChild = BinaryTree(newNode)
        else:
            t = BinaryTree(newNode)
            t.leftChild = self.leftChild
            self.leftChild = t

    def insertRight(self, newNode):

        if self.rightChild is None:
            self.rightChild = BinaryTree(newNode)
        else:
            t = BinaryTree(newNode)
            t.rightChild = self.rightChild
            self.rightChild = t

    def getRightChild(self):
        return self.rightChild

    def getLeftChild(self):
        return self.leftChild

    def setRootval(self, obj):
        self.key = obj

    def getRootVal(self):
        return self.key


def buildTree():
    r = BinaryTree('a')
    r.insertLeft('b')
    r.insertRight('c')
    r.getLeftChild().insertRight('d')
    r.getRightChild().insertRight('f')
    r.getRightChild().insertLeft('e')
    return r


ttree = buildTree()

testEqual(ttree.getRightChild().getRootVal(), 'c')
testEqual(ttree.getLeftChild().getRightChild().getRootVal(), 'd')
testEqual(ttree.getRightChild().getLeftChild().getRootVal(), 'e')
