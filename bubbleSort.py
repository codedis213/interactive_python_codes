import random


def bubbleSort(items_list):
    for n in range(items_list.__len__() - 1, 0, -1):
        for index in range(n):
            if items_list[index] > items_list[index + 1]:
                items_list[index], items_list[index + 1] = items_list[index + 1], items_list[index]

    return items_list


if __name__ == "__main__":
    alist = [54, 26, 93, 17, 77, 31, 44, 55, 20]
    bubbleSort(alist)
    print(alist)

    my_randoms = random.sample(xrange(100000), 100000)
    print my_randoms
    bubbleSort(my_randoms)
    print(my_randoms)
