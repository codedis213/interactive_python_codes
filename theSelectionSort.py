def selectionSort(unsortList):
    for i in range(unsortList.__len__() - 1, 0, -1):
        largerNoIndex = 0
        for index in range(1, i + 1):
            if unsortList[index] > unsortList[largerNoIndex]:
                largerNoIndex = index

        unsortList[i], unsortList[largerNoIndex] = unsortList[largerNoIndex], unsortList[i]
        print unsortList


if __name__ == "__main__":
    alist = [11, 7, 12, 14, 19, 1, 6, 18, 8, 20]
    selectionSort(alist)
