def BinaryTree(r):
    return [r, [], []]


def insertLeft(root, newBranach):
    t = root.pop(1)

    if t:
        root.insert(1, [newBranach, t, []])
    else:
        root.insert(1, [newBranach, [], []])

    return root


def insertRight(root, newBranch):
    t = root.pop(2)

    if t:
        root.insert(2, [newBranch, [], t])
    else:
        root.insert(2, [newBranch, [], []])

    return root


def getRootVal(root):
    return root[0]


def setRootVal(root, newVal):
    root[0] = newVal


def getLeftChild(root):
    return root[1]


def getRightChild(root):
    return root[2]


r = BinaryTree(3)
insertLeft(r, 4)
insertLeft(r, 5)
insertRight(r, 6)
insertRight(r, 7)
l = getLeftChild(r)
print(l)

setRootVal(l, 9)
print(r)
insertLeft(l, 11)
print(r)
print(getRightChild(getRightChild(r)))

print "###########"

x = BinaryTree('a')
insertLeft(x, 'b')
insertRight(x, 'c')
insertRight(getRightChild(x), 'd')
insertLeft(getRightChild(getRightChild(x)), 'e')

print x

print "#"*10

x = BinaryTree('a')
insertLeft(x, 'b')
insertRight(x, 'c')
insertRight(getLeftChild(x), 'd')
insertLeft(getRightChild(x), 'e')
insertRight(getRightChild(x), 'f')

print x

insertLeft(getRightChild(getRightChild(x)), 'e')


